Title: Getting Microsoft Teams Optimization to Work on Citrix Workspace Running on Debian 11 Bullseye
Date: 2021-12-15 12:42
Tags: Citrix

# Resources

Some of the system requirements for MS Teams optimization: https://docs.citrix.com/en-us/citrix-virtual-apps-desktops/multimedia/opt-ms-teams.html#system-requirements

(All system requirements: https://docs.citrix.com/en-us/citrix-workspace-app-for-linux/system-requirements.html#requirements)

Extra configuration & adding libunwind-12: https://docs.citrix.com/en-us/citrix-workspace-app-for-linux/configure-xenapp.html#optimization-for-microsoft-teams

How to check it is running: https://www.citrix.com/blogs/2020/06/30/microsoft-teams-is-now-available-in-citrix-workspace-app-for-linux/

# Instructions

- Made sure all the dependencies for MS Teams optimization are installed. Most of them were already there.
  - Installed all versions / variations of libstdc++ and libc++ >= 9
  - Installed libunwind-12 following above instructions
- Logged out of virtual desktop & rebooted client machine
