Title: How to Create a Wireless Printer Using Raspberry Pi 1 B
Date: 2021-02-28 18:45

- If it is a new Raspberry Pi, install the latest Raspberry Pi OS on it and complete the initial setup.
- Then reserve the Raspberry Pi's IP address for it in the router settings to make sure it always gets the same IP.
- Make sure everything is up to date:
  ```
  sudo apt-get update
  sudo apt-get dist-upgrade
  ```
- Secure the Pi following <https://www.raspberrypi.org/documentation/configuration/security.md>:
  - Create a user for SSH access that is different than the built-in `pi`.
    ```
    sudo adduser <username>
    sudo usermod -a -G adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,gpio,i2c,spi <username>
    ```
  - Make sure a password is required for sudo:
    ```
    sudo visudo /etc/sudoers.d/010_pi-nopasswd
    ```
  - Change the SSH port from 22 to something else in `/etc/ssh/sshd_config` for added security. Then restart the SSH service.
    Make sure that you also use this different SSH port when following the other instructions on the page.
  - Configure SSH in ufw:
    ```
    sudo ufw allow <new SSH port>
    sudo ufw limit <new SSH port>/tcp
    ```
  - Install fail2ban & configure it with the new SSH port following the above page. Then restart it.
- Optionally, install the BOINC client to make sure the CPU is put to good use when the Pi is not being used otherwise:
  ```
  sudo apt install boinc-client boinctui
  boinctui  # configure BOINC
  ``` 
- Install CUPS if not present already.
  Also install the following:
  ```
  sudo ufw allow cups
  ```
- If you're logged in via the desktop, open the printing settings and enable remote administration.
  - Add CUPS admin privileges the the user we've just created:
    ```
    sudo usermod -a -G lpadmin <username>
    ```
    Now you can login to the CUPS admin interface with these logon details.
  - Now you can access the admin page at: `http://<IP of Pi>:631/` and add the connected printer there.

Sources:

- <https://raspberrypi.stackexchange.com/questions/27496/samsung-multifunction-printer-with-cups-failing-to-print#answer-112770>
- <https://www.raspberrypi.org/documentation/configuration/security.md>
- <https://medium.com/@jasonrigden/a-guide-to-the-uncomplicated-firewall-ufw-for-linux-570c3774d7f4>
