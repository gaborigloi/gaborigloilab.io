Title: Running MrBayes
Date: 2016-02-20 12:45
Category: MrBayes

##Running commands in batch, see <http://mrbayes.sourceforge.net/mb3.2_manual.pdf#120>
*All the files mentioned here can be viewed at <https://gaborigloi.gitlab.io/mrbayes>*
###Running the program with commands for primates.mex example dataset:
```bash
# method 1 
mb commands.nex > log.txt
```
We redirect the standard output of MrBayes to the file `log.txt` using the `>`{.bash} symbol. What the program would normally write to the terminal will now be written to `log.txt`. We simply pass the name of the Nexus batch file we want to run ([`commands.nex`](https://gaborigloi.gitlab.io/mrbayes/commands.nex)) as a command-line argument to the program.
```bash
# method 2
mb < batch.txt > log.txt
```
In `method 2`, we also redirect the standard input of MrBayes to come from the file [`batch.txt`](https://gaborigloi.gitlab.io/mrbayes/batch.txt) using the `<`{.bash} symbol. Then the program will behave as if we typed in all the commands found in `batch.txt` manually into ther terminal of MrBayes.

###Run multiple nex command files with `nohup`{.bash} in background and leave ssh session:

We use the `&`{.bash} symbol to make the program launched with nohup a background job, and follow this command by `exit`{.bash}, which terminates the SSH session:
```bash
nohup mb commands.nex commands.nex > log.txt & exit
```

###Running the parallel (MPI) version of MrBayes

Similarly, we can run the nex command file with the MPI version of MrBayes on the 2 available cores:

```bash
nohup mpirun -np 2 pmb commands.nex > log.txt & exit
```
Using both cores can speed up computation by a factor of two, but while running this version with the primates example dataset, many "`Gamma parameter less than zero`" messages were printed, both on my laptop and the MCS machine.

You can download the parallel MPI version of MrBayes from <https://gaborigloi.gitlab.io/mrbayes/bin/pmb>. Just put it into the `bin` directory in your home directory (next to `mb`), the `pmb` command will be available everywhere since `~/bin` is already in the `PATH`.

To get the number of cores, run the `nproc`{.bash} command:
(<http://www.cyberciti.biz/faq/linux-get-number-of-cpus-core-command/>)
```bash
nproc
```
**For running with MPI see <http://mrbayes.sourceforge.net/mb3.2_manual.pdf#124> and <http://mrbayes.sourceforge.net/mb3.2_manual.pdf#132>**

###Viewing the log of a background process

You can view the log updated as it is written to by MrBayes using `tail -f log.txt` as described in page 132 of the manual linked above. The `-f` option ("follow") tells `tail` to display new lines of the file as it grows. To quit the `tail` program, press CTRL-C.

##Runtime of different program versions with primates.nex example file:
Measuring runtime on MCS (or DS?) machine using the `time`{.bash} command:
```bash
time mb commands.nex > log.txt
```
The output of the command:
```

real    0m19.669s
user    0m19.032s
sys     0m0.144s
```
On MCS machine with MPI (two cores as nproc command returned 2):

 - produced many "`Gamma parameter less than zero`" messages

```bash
$ time mpirun -np 2 pmb commands.nex > log.txt
```
```
real    0m11.508s
user    0m20.328s
sys     0m0.276s
```

On my laptop, without MPI, using one processor core:
```bash
$ time mb commands.nex > log.txt
```
```
real    0m42.021s
user    0m41.496s
sys     0m0.308s
```
On my laptop, with MPI, using both processor cores:

 - also produced many "`Gamma parameter less than zero`" messages

```bash
$ time mpirun -np 2 ../bin/pmb commands.nex > log.txt
```
```
real    0m24.049s
user    0m42.620s
sys     0m0.700s
```
