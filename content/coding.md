Title: Tips for Your First Programming Job
Date: 2018-03-30 12:45

Here are some general good practices that I did not know when I started working as a programmer, but now I find essential:

* **The less code, the better:**
  The company will have to maintain all that code...
  In general, I found minimalism a good programming principle to follow, the
[YAGNI] principle spells this out. We cannot foresee the future, so it is best
  to make our code specific to solve the problems we have at hand, instead of
  making it generally work for some hypothetical future use cases.
* **The simpler the code, the better:**
  The code should be easy to understand: somebody will have to review and debug it...
* **Make your work more efficient** by reducing its repetitious aspects:
  Using scripts will help you get more work done with less typing. The [fish shell]
  is also very helpful for this, it has advanced history-based
  autosuggestion. Another thing I found essential for a programming job is touch
  typing.
* **Read:**
  Gaining background knowlege will help understand the bigger picture.
  I found The Elements of Computing Systems a useful book for learning how
  computers work from the hardware up to the software, the book teaches the reader how to
  "build a modern computer from frist principles".
  Another book I found useful is The Turing Omnibus, it offers a good
  introduction to Computer Science and covers a wide range of topics.
* **Take care:**
  Lastly, it is important to be aware of the negative health, etc. aspects of
  programming, a lifestyle without a good work-life balance and enough exercise
  is not really sustainable on the long term.
  Another important thing is to avoid getting addicted to coffee (or something
  else) - a few of my
  friends struggle with this. It's better to pay attention early to prevent
  developing an addiction.

[fish shell]: https://fishshell.com/
[YAGNI]: https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it
