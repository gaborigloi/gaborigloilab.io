Title: Treasure chest craft for Colossians 2:1-7
Date: 2023-11-11 14:56
Category: Crafts
Tags: Bible, craft
Slug: colossians-2-treasure-chest-craft
Authors: Iglói Gábor
Summary: Treasure chest craft for teaching Colossinas 2:1-7 to children - In Christ are hidden all the treasures of wisdom and knowledge

Below you can find a fun treasure chest craft that I have used for teaching Colossians 2:1-7 to children - In Christ are hidden all the treasures of wisdom and knowledge.

The document can be printed two-sided so that, when assembled, the treasures and the Bible verse will appear inside the treasure chest when it's opened.

For cutting out the shape, just follow the thick black lines. After the children have coloured in the different sides and parts of the treasure chest, it can be folded by the thin blue lines and then assembled without glueing.

[Download 2-sided PDF: Treasure chest craft for Colossinas 2:1-7]({static}/pdfs/colossians_2_treasure_chest_craft.pdf)

![Preview of printable Colossians 2 treasure chest craft]({static}/images/colossians_2_treasure_chest_craft_assembly/colossians_2_treasure_chest_craft_pdf_preview.png)

## Pictures - assembling the craft

![Top side of the printed craft PDF document]({static}/images/colossians_2_treasure_chest_craft_assembly/craft_top.JPG)

![Other side of the printed craft PDF document]({static}/images/colossians_2_treasure_chest_craft_assembly/craft_other_side.JPG)

![Top side of the printed craft PDF document after cutting and colouring]({static}/images/colossians_2_treasure_chest_craft_assembly/craft_cut_top.JPG)

![Other side of the printed craft PDF document after cutting and colouring]({static}/images/colossians_2_treasure_chest_craft_assembly/craft_cut_other_side.JPG)

![Assembled treasure chest craft]({static}/images/colossians_2_treasure_chest_craft_assembly/craft_assembled.JPG)

![Assembled treasure chest craft, open, showing the treasures inside]({static}/images/colossians_2_treasure_chest_craft_assembly/craft_assembled_open.JPG)
