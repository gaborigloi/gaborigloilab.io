Title: How to Install Xerox Phaser 3010 Printer on Debian Jessie
Date: 2017-02-20 12:45

These instructions might still work with newer Debian releases, such as Debian
Stretch, and also with other Debian-based distributions, such as Ubuntu or
Linux Mint. The troubleshooting methods explained here can certainly be used to get the driver to work with Debian derivatives.

First we download and install the package provided by Xerox, and install some dependencies suggested by various blog posts:

```
# Search for your printer on the Xerox website to find the latest package providing the driver.
wget download.support.xerox.com/pub/drivers/3010/drivers/linux/en_GB/xerox-phaser-3010-3040_1.0-28_i386.zip
unzip xerox-phaser-3010-3040_1.0-28_i386.zip
sudo dpkg --add-architecture i386
sudo dpkg -i xerox-phaser-3010-3040_1.0-28_i386.deb
sudo apt-get install -f
sudo apt-get install libcups2:i386 libcupsfilters1:i386 libcupsimage2:i386
```

On my system these dependencies were not sufficient, the driver still did not work.

Let's see what files the newly-installed package provides:

```
dpkg -L xerox-phaser-3010-3040
```

One of the files provided by the package was `/usr/lib/xrhr1acl.so`.
We can use `ldd` to see if there are any missing shared libraries:

```
ldd /usr/lib/xrhr1acl.so
```

Indeed, in my case, `libstdc++` seemed to be missing. We can find out which package provides that shared library using `apt-file`, and then install the missing packge:

```
apt-file update
apt-file find libstdc++.so.6
sudo apt-get install lib32stdc++6
```

Now we can verify by running `ldd` again that all the required shared libraries are present:
```
ldd /usr/lib/xrhr1acl.so
```

Now that the problems have been fixed, we finally restart `cups`:
```
sudo service cups restart
```
